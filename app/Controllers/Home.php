<?php

namespace App\Controllers;

use \App\Models\TweetModel;

class Home extends BaseController
{
    var $config = null;
    var $categories = null;
    var $tweetMdl;
    var $sess;
    var $curUser;

    public function __construct()
    {
        $this->config = new \Config\AdtConfig();
        $this->categories = $this->config->getCategories();
        $this->tweetMdl = new TweetModel();
        $this->sess = session();
        $this->curUser = $this->sess->get('currentuser');
    }
    
    public function index()
    {
        $userMdl = new \App\Models\UserModel();
        
        $data['categories'] = $this->categories;
        $data['tweets'] = $this->tweetMdl->getLatest();
        $data['profile'] = $userMdl->find($this->curUser['userid']);
        $data['judul'] = 'Tweet Terbaru';

        // var_dump($data['profile']);

        return view('components/header')
                .view('tweet_home', $data)
                .view('components/footer');
    }

    public function category($category)
    {
        $userMdl = new \App\Models\UserModel();

        $data['categories'] = $this->categories;
        $data['tweets'] = $this->tweetMdl->getByCategory($category);
        $data['profile'] = $userMdl->find($this->curUser['userid']);
        $data['judul'] = 'Tweet Kategori #'.$category;

        return view('components/header')
                .view('tweet_home', $data)
                .view('components/footer');
    }

    public function addForm()
    {
        $data['categories'] = $this->categories;
        return view('components/header')
                .view('tweet_add', $data)
                .view('components/footer');
    }

    public function addTweet()
    {
        $this->tweetMdl->newTweet($this->sess->get('currentuser'), $this->request->getPost());
        $this->sess->setFlashdata('addtweet', 'success');
        return redirect()->to('/');
    }

    public function delTweet($tweet_id)
    {
        $result = $this->tweetMdl->delTweet($this->sess->get('currentuser')['userid'], $tweet_id);
        if($result){
            $this->sess->setFlashdata('deltweet', 'success');
        } else {
            $this->sess->setFlashdata('deltweet', 'error');
        }
        return redirect()->to('/');
    }

    public function editForm($tweet_id)
    {
        $tweet = $this->tweetMdl->find($tweet_id);
        if($tweet->user_id != $this->sess->get('currentuser')['userid']){
            $this->sess->set('edittweet', 'error');
            return redirect()->to('/');
        }
        
        $data['categories'] = $this->categories;
        $data['tweet'] = $tweet;
        return view('components/header')
                .view('tweet_edit', $data)
                .view('components/footer');
    }

    public function editTweet()
    {
        $result = $this->tweetMdl->editTweet($this->request->getPost());
        if($result){
            $this->sess->setFlashdata('edittweet', 'success');
        } else {
            $this->sess->setFlashdata('edittweet', 'error');
        }

        return redirect()->to('/');
    }
}
